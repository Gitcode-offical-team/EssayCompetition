# 一、活动介绍
GitCode平台汇聚了众多杰出的G-Star项目，它们犹如璀璨星辰，用各自的故事和成就，为后来者照亮前行的道路。我们诚邀广大开发者、项目维护者及爱好者，共同撰写并分享项目在GitCode平台上托管的体验，挖掘平台玩法，让更多人了解并受益于这些宝贵的资源。

入选投稿将在GitCode官方媒体广泛传播，获得丰厚的奖品。通过“光引计划”，我们期望能够提升项目的曝光度，吸引更多关注与支持，同时收集宝贵的用户反馈，以持续优化平台服务，为开发者们提供更加高效、便捷的创作与发展环境。让我们携手并进，在G-Star项目星光的指引下，共同探索开源的无限可能！
## 征集对象
面向所有阶段的开源项目，新开源项目也可参加
## 征文用途
- 通过初审的投稿将发布在GitCode官方渠道进行展示，供广大用户参考
- 终审排名前10%的投稿将引用在GitCode G-Star案例页面
## 征文内容要求
1. 项目投稿前必须先通过G-Star认证
2. 投稿内容需为原创且在 GitCode 首发
3. 投稿内容需包含：
-   1. 项目介绍：详细阐述项目的背景、目标、功能特色及已实现的关键里程碑，项目在GitCode的仓库地址。
-   2. 社区化程度：明确说明项目当前的社区化状况，包括核心成员数量、贡献者数量以及社区活跃度（如Issue讨论量、Pull Request合并量等）。
-   3. GitCode平台体验感受：分享项目在GitCode平台上的使用体验，包括但不限于代码托管、项目成员协作、社区互动等方面的感受与建议。
-   4. 项目期望：阐述项目未来在GitCode平台上希望实现的目标，包括但不限于功能扩展、社区建设、合作伙伴招募等。
-   5.不少于4张配图，如：项目logo、项目技术架构图、GitCode产品体验界面截图等。

# 二、投稿时间与流程
![image.png](https://raw.gitcode.com/Gitcode-offical-team/EssayCompetition/attachment/uploads/5c76182d-0d0c-4ec6-885d-8a7b0e052a83/image.png 'image.png')
1. 投稿期：2024年10月31日 - 2024年12月31日
## 2. 投稿方式
- 步骤一：将开源项目导入GitCode平台并申请G-Star认证
- 申请地址：https://gitcode.com/g-star/apply
- 步骤二：参与者在活动仓库https://gitcode.com/Gitcode-offical-team/EssayCompetition内创建一个新的Issue，标题以“[光引计划] +项目名称”开头，并在Issue正文中按照上述要求填写投稿内容。
3. 评审周期：每周进行一次评审（每周五为评审日），符合要求的投稿，issue将被关闭，并发放奖品；未通过评审的投稿，官方将在评论给予反馈意见，投稿者可根据意见进行修改后重新提交；2025年1月1日-1月7进行终审评选，根据终审排名评选出10篇最佳案例。
4. 总结颁奖：活动结束后一周内，在活动群和官方公众号公布最终获奖名单，发放奖品。
# 三、终审评分标准
![image.png](https://raw.gitcode.com/Gitcode-offical-team/EssayCompetition/attachment/uploads/e5706910-06e7-41cf-a0f5-e753ab5f1be7/image.png 'image.png')
## 特邀评审嘉宾介绍
![光引计划-4.png](https://raw.gitcode.com/Gitcode-offical-team/EssayCompetition/attachment/uploads/6efb1a33-926a-4188-8c40-5128131b3a54/光引计划-4.png '光引计划-4.png')
# 四、奖项设置
- Super Star：超级之星
奖品：Apple AirPods Pro (第二代) 数量：1  
获取方式：终审第一名
- Guide Star：领航之星 
奖品：索尼 无线降噪立体声耳机 数量：2  
获取方式：终审第二、三名
- Dream Star：逐梦之星 
奖品：CHERRY mx3.0s 游戏办公机械键盘 数量：7  
获取方式：终审第四-十名
- 阳光普照奖：100 面值京东卡 数量：100  
获取方式：投稿通过初审，一位作者仅可获得一张京东卡激励，活动结束后发放。
- 锦鲤奖：周边大礼包 数量：1  
获取方式：投稿通过初审，活动结束后抽取。

# 五、授权声明
提交作品即视为您授权给GitCode平台在活动期间及活动结束后，有权对作品进行必要的编辑、修改、翻译、发表和传播。这包括但不限于在主办方的官方网站、社交媒体平台、出版物及其他官方渠道上使用。

# 六、获奖结果公示

**一等奖**

《你只负责维护实体，数据表的事情交给我》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/20

**二等奖**

《DyJava：让抖音开发更简单，GitCode助力更高效》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/46

《Easy-Es，用了皆欢喜！》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/45

**三等奖**

《Dora SSR 的奇妙开源之旅》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/33

《Go IoT，高可用物联网的卓越之选》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/9

《至善云学》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/37

《python-office：一行代码，实现自动化办公》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/31

《JeeSite 快速开发平台：全能企业级快速开发解决方案》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/4

《vue3-element-admin：极简开箱即用的企业级后台管理前端模板》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/17

《洛书：超轻量级跨平台脚本语言》

https://gitcode.com/GitCode-official-team/EssayCompetition/issues/56

**阳光普照奖：**
所有通过初审的项目均可获得100元京东卡一张

**锦鲤奖获奖项目：**
SnailJob，奖品为GitCode周边大礼包一份

最终解释权：本声明的最终解释权归主办方所有。





